// Setting up dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors");
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes");
const app = express();

mongoose.connect("mongodb+srv://admin:admin123@zuitt.oit04zm.mongodb.net/e-commerce?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes)
app.use("/products", productRoutes)

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is not online on port ${process.env.PORT || 4000}`)
})